<?php
/**
 * The template used for displaying page content in content_box.php
 *
 * @package UCD_WPtheme
 */
?>

<div class="repeating_content_block clear_below" id="post-<?php the_ID(); ?>">
<h3><?php the_title(); ?></h3>
        <div class="content_box right">
	 <div class="border_box">
	   <h3 class="headline headline_top"><?php echo get_post_meta(get_the_ID(),'_ucd_content_box_title', true); ?></h3>
	   <?php $content_box_content = get_post_meta(get_the_ID(),'_ucd_content_box_content', true); ?>
	   <?php $content_box_content = apply_filters('the_content', $content_box_content); ?>
	   <p><?php echo wpautop($content_box_content); ?></p>
	 </div>
        </div>
	<?php the_content(); ?>
<?php edit_post_link( __( 'Edit', 'ucd' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
</div>
