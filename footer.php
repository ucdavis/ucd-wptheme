<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package UCD-WPtheme
 */
?>

  </div><!-- #content -->

<!-- ******************** Footer ******************** -->
<div id="footer">
  <div class="outer_wrap">
      <div class="inner_wrap">
          <div id="footer_content">
              <!-- ********** Optional footer links ********** -->
              
              <!-- ********** End optional footer links ********** -->

              <!-- ********** Optional footer extras ********** -->
              
              <!-- ********** End optional footer extras ********** -->

              <!-- ********** Standard footer content ********** -->
              <div id="standard_footer" role="contentinfo">
                <?php $options = get_option('ucd_theme_settings'); ?>
          <?php if($options){
           if($options['footer_html']!="") {echo $options['footer_html'];} 
           else {
           echo '<div class="footer-logo"><a href="' .
           esc_url( home_url( "/" ) ); 
           echo '" title=';
           echo esc_attr( get_bloginfo( "name", "display" ) );
           echo ' rel="home">'; 
           echo esc_attr( get_bloginfo( "name", "display" ) );
           echo '</a></div><p><a href="mailto:';
           echo esc_attr( get_bloginfo( "admin_email", "display" ) );
           echo '">Questions and comments?</a> | <a href="http://ucdavis.edu/help/privacy-accessibility.html">Privacy &amp; Accessibility</a> | Last update: Jan. 22, 2014</p>';
           }
          }?>
                <br>
              </div>
              <!-- ********** End standard footer content ********** -->
          </div>
      </div>
  </div>
</div>
<!-- ******************** End footer ******************** -->
<?php wp_footer(); ?>

</body>
</html>